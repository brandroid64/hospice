-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: hospice
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `certification`
--

DROP TABLE IF EXISTS `certification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `demo_id` int(11) DEFAULT NULL,
  `cert_num` int(11) DEFAULT NULL,
  `cert_type` varchar(20) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `cert_signed` varchar(30) DEFAULT NULL,
  `cert_physician1` varchar(50) DEFAULT NULL,
  `cert_physician2` varchar(50) DEFAULT NULL,
  `transfer_in` tinyint(4) DEFAULT NULL,
  `f2f` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certification`
--

LOCK TABLES `certification` WRITE;
/*!40000 ALTER TABLE `certification` DISABLE KEYS */;
/*!40000 ALTER TABLE `certification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demographics`
--

DROP TABLE IF EXISTS `demographics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demographics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(80) DEFAULT NULL,
  `middle_initial` varchar(1) DEFAULT NULL,
  `last_name` varchar(80) DEFAULT NULL,
  `mr_num` varchar(20) DEFAULT NULL,
  `admit_type` enum('','New Admit','Re-Admit','Transfer') DEFAULT 'New Admit',
  `flags` varchar(40) DEFAULT NULL,
  `cti` tinyint(4) DEFAULT NULL,
  `noe` tinyint(4) DEFAULT NULL,
  `81a` tinyint(4) DEFAULT NULL,
  `81b` tinyint(4) DEFAULT NULL,
  `soc` date DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `discharged` datetime DEFAULT NULL,
  `sex` enum('','F','M') DEFAULT '',
  `birthdate` date DEFAULT NULL,
  `ssn` varchar(12) DEFAULT NULL,
  `race` varchar(30) DEFAULT NULL,
  `religion` varchar(30) DEFAULT NULL,
  `language` varchar(30) DEFAULT NULL,
  `marital` varchar(20) DEFAULT NULL,
  `ethnicity` varchar(30) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `cell` varchar(20) DEFAULT NULL,
  `other_number` varchar(20) DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL,
  `gate_code` varchar(20) DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `state` varchar(5) DEFAULT NULL,
  `zip` varchar(11) DEFAULT NULL,
  `county` varchar(40) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demographics`
--

LOCK TABLES `demographics` WRITE;
/*!40000 ALTER TABLE `demographics` DISABLE KEYS */;
INSERT INTO `demographics` VALUES (1,'Burdean','F','Oliverson','123456','New Admit',NULL,NULL,NULL,NULL,NULL,'2016-06-21','Active',NULL,'M','1976-05-16','808-14-8008','White','LDS','Engrish','Married','Non-Hispanic',NULL,NULL,NULL,NULL,'24 Cedar Crest Ct',NULL,NULL,'Newbury Park','CA','91320',NULL,1,'2016-05-22 01:44:40');
/*!40000 ALTER TABLE `demographics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnosis`
--

DROP TABLE IF EXISTS `diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnosis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `demo_id` int(11) DEFAULT NULL,
  `source` enum('ICD10 PRIMARY','ICD9 PRIMARY','ICD10 ADDITIONAL','ICD9 ADDITIONAL') DEFAULT NULL,
  `diagnosis` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnosis`
--

LOCK TABLES `diagnosis` WRITE;
/*!40000 ALTER TABLE `diagnosis` DISABLE KEYS */;
INSERT INTO `diagnosis` VALUES (1,1,'ICD10 PRIMARY','C021  Malignant neoplasm of border of tongue',NULL,1,'2016-05-22 02:14:01'),(2,1,'ICD9 PRIMARY','331.0 ES ALZHEIMER\'S',NULL,1,'2016-05-22 02:14:23');
/*!40000 ALTER TABLE `diagnosis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurance`
--

DROP TABLE IF EXISTS `insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `demo_id` int(11) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `note` text,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurance`
--

LOCK TABLES `insurance` WRITE;
/*!40000 ALTER TABLE `insurance` DISABLE KEYS */;
/*!40000 ALTER TABLE `insurance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `demo_id` int(11) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `facility` varchar(60) DEFAULT NULL,
  `npi` varchar(30) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `notes` mediumtext,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(80) DEFAULT NULL,
  `middle_initial` varchar(1) DEFAULT NULL,
  `last_name` varchar(80) DEFAULT NULL,
  `mr_num` varchar(20) DEFAULT NULL,
  `admit_type` enum('','New Admit','Re-Admit','Transfer') DEFAULT 'New Admit',
  `flags` varchar(40) DEFAULT NULL,
  `cti` varchar(1) DEFAULT NULL,
  `noe` varchar(1) DEFAULT NULL,
  `81a` varchar(1) DEFAULT NULL,
  `81b` varchar(1) DEFAULT NULL,
  `soc` date DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `discharged` datetime DEFAULT NULL,
  `sex` enum('','F','M') DEFAULT '',
  `birthdate` date DEFAULT NULL,
  `ssn` varchar(12) DEFAULT NULL,
  `race` varchar(30) DEFAULT NULL,
  `religion` varchar(30) DEFAULT NULL,
  `language` varchar(30) DEFAULT NULL,
  `marital` varchar(20) DEFAULT NULL,
  `ethnicity` varchar(30) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `cell` varchar(20) DEFAULT NULL,
  `other_number` varchar(20) DEFAULT NULL,
  `address` varchar(80) DEFAULT NULL,
  `gate_code` varchar(20) DEFAULT NULL,
  `mileage` int(11) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `state` varchar(5) DEFAULT NULL,
  `zip` varchar(11) DEFAULT NULL,
  `county` varchar(40) DEFAULT NULL,
  `active` varchar(1) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'Burdean','F','Oliverson','123456','New Admit',NULL,'',NULL,NULL,NULL,'2016-06-21','Active',NULL,'M','1976-05-16','808-14-8008','White','LDS','Engrish','Married','Non-Hispanic','',NULL,NULL,NULL,'24 Cedar Crest Ct',NULL,NULL,'Newbury Park','CA','91320',NULL,'1','2016-06-22 02:14:40'),(2,'Brandon',NULL,'Bowles','1234','New Admit',NULL,NULL,NULL,NULL,NULL,NULL,'Pending',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2016-06-22 01:42:36'),(3,'Brandon',NULL,'Bowles','12345','New Admit',NULL,NULL,NULL,NULL,NULL,NULL,'Pending',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1','2016-06-22 02:17:34');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_certification`
--

DROP TABLE IF EXISTS `patient_certification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_certification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `signed` varchar(30) DEFAULT NULL,
  `physician1` varchar(50) DEFAULT NULL,
  `physician2` varchar(50) DEFAULT NULL,
  `transfer_in` varchar(1) DEFAULT NULL,
  `f2f` varchar(1) DEFAULT NULL,
  `active` varchar(1) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_certification`
--

LOCK TABLES `patient_certification` WRITE;
/*!40000 ALTER TABLE `patient_certification` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_certification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_diagnosis`
--

DROP TABLE IF EXISTS `patient_diagnosis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_diagnosis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `source` enum('ICD10 PRIMARY','ICD9 PRIMARY','ICD10 ADDITIONAL','ICD9 ADDITIONAL') DEFAULT NULL,
  `diagnosis` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_diagnosis`
--

LOCK TABLES `patient_diagnosis` WRITE;
/*!40000 ALTER TABLE `patient_diagnosis` DISABLE KEYS */;
INSERT INTO `patient_diagnosis` VALUES (1,1,'ICD10 PRIMARY','C021  Malignant neoplasm of border of tongue','2014-04-09',1,'2016-05-23 15:59:11'),(2,1,'ICD9 PRIMARY','331.0 ES ALZHEIMER\'S',NULL,1,'2016-05-22 02:14:23');
/*!40000 ALTER TABLE `patient_diagnosis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_insurance`
--

DROP TABLE IF EXISTS `patient_insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_insurance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `number` varchar(30) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `note` text,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_insurance`
--

LOCK TABLES `patient_insurance` WRITE;
/*!40000 ALTER TABLE `patient_insurance` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_insurance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_location`
--

DROP TABLE IF EXISTS `patient_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `facility` varchar(60) DEFAULT NULL,
  `npi` varchar(30) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `notes` mediumtext,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_location`
--

LOCK TABLES `patient_location` WRITE;
/*!40000 ALTER TABLE `patient_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_physician`
--

DROP TABLE IF EXISTS `patient_physician`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_physician` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `npi` varchar(30) DEFAULT NULL,
  `notify` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_physician`
--

LOCK TABLES `patient_physician` WRITE;
/*!40000 ALTER TABLE `patient_physician` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_physician` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physician`
--

DROP TABLE IF EXISTS `physician`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physician` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `demo_id` int(11) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `npi` varchar(30) DEFAULT NULL,
  `notify` tinyint(4) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physician`
--

LOCK TABLES `physician` WRITE;
/*!40000 ALTER TABLE `physician` DISABLE KEYS */;
/*!40000 ALTER TABLE `physician` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physicians`
--

DROP TABLE IF EXISTS `physicians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physicians` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `npi` varchar(30) DEFAULT NULL,
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physicians`
--

LOCK TABLES `physicians` WRITE;
/*!40000 ALTER TABLE `physicians` DISABLE KEYS */;
/*!40000 ALTER TABLE `physicians` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-17 11:53:59
